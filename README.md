#SiKantin
[![pipeline status](https://gitlab.com/ppw-2019-kb07/sikantin-2.0/badges/master/pipeline.svg)](https://gitlab.com/ppw-2019-kb07/sikantin-2.0/commits/master)
[![coverage report](https://gitlab.com/ppw-2019-kb07/sikantin-2.0/badges/master/coverage.svg)](https://gitlab.com/ppw-2019-kb07/sikantin-2.0/commits/master)

Tugas Kelompok 2 PPW 2019

Kelompok: KB07

Link website: [Sikantin](https://sikantin.herokuapp.com)

## Anggota

1. Mahardika Krisna Ihsani (1806141284)
2. Putri Salsabila (1806186805)
3. Muhammad Ramy Azhar (1806191446)
4. Sean Zeliq Urian (1806141454)

## Deskripsi Website
Saat ini, kepadatan Fasilkom UI terutama ketika jam makan siang semakin pada setiap tahunnya, terutama dengan bertambahnya jumlah mahasiswa yang meningkat setiap tahunnya. Keadaan tersebut tentunya dapat berdampak pada beberapa elemen Fasilkom seperti orang mengantri makanan dengan waktu yang lumayan lama. Padahal tidak semua dari mereka mempunyai waktu yang cukup, terutama untuk mahasiswa yang mempunyai waktu isoma yang relatif singkat. Selain itu, pembeli harus melihat-lihat makanan yang tersebut terlebih dahulu sehingga waktu yang terpakai lumayan banyak. Dari permasalahan tersebut, kelompok kami berinisiatif untuk membuat SiKantin. SiKantin merupakan website sistem informasi kantin fasilkom UI. Mengingat tema yang digunakan revolusi industri 4.0, kami mengutilisasi suatu recommender system yang dapat merekomendasikan makanan yang dapat dibeli sehingga pembeli dapat menghemat waktu untuk memilih makanan. 

## Manfaat Website
1. Elemen Fasilkom : Tidak perlu mengantri untuk membeli makanan sehingga dapat mengefisiensi waktu
2. Penjual: Memperoleh laba lebih besar, karena waktu yang dibutuhkan untuk memrproses pemesanan semakin berkurang

## Daftar Fitur untu Tugas Kelompok 2
1. Calorie Counter & Food Search: Berguna untuk menghitung kalori makanan sekaligus fitur untuk memberi tahu berapa kuota kalori yang diperlukan seseorang (Calorie Counter) dan mencari tahu makanan berdasarkan nama makanannya dan kalori minimumnya (Food Search) - Mahardika Krisna Ihsani

2. Feedback: Fitur untuk memberi masukan dan testimoni website (Putri Salsabila)

3. Pemesanan: Fitur untuk melakukan pemesanan makanan (Sean Zeliq Urian)

4. History/Riwayat: Fitur untuk mengecek daftar pemesanan yang dilakukan oleh suatu client (M. Ramy Azhar)


## FAQ
1. Apa itu sikantin? Sikantin adalah sistem informasi kantin Fasilkom UI, jadi bisa pesan makanan tanpa datang langsung ke tokonya.
2. Apakah hanya anak fasilkom yang boleh memesan lewat Sikantin? tidak dong, siapapun boleh memesan tapi jangan lupa daftarkan diri terlebih dahulu ya!
3. Apakah bisa diantar pesananannya? Ya, saat di halaman pembayaran bisa memilih untuk diantarkan atau diambil langsung.
4. Apakah saya bisa memesan makanan dari kantin fakultas lain(bukan fasilkom)? untuk saat ini hanya kantin fasilkom dulu ya!


