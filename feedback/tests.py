from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from .forms import FeedbackForm
from .models import SavedFeedback


class Unittest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            "testuser", "johndoe@foo.bar", "pepewe123", first_name="First", last_name="Last")
        self.client = Client()
        self.client.login(username="testuser", password="pepewe123")

    def test_form_page(self):
        response = self.client.get('/feedback/')
        self.assertEqual(response.status_code, 200)

    def test_form_submit(self):
        response = self.client.post(
            '/feedback/', {'testimony': '10/10 gonna use this site again', 'feedback': 'lanjutkeun'})
        feedback = SavedFeedback.objects.get(pk=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(feedback.testimony, '10/10 gonna use this site again')
        self.assertEqual(feedback.feedback, 'lanjutkeun')

    def test_page_error(self):
        response = self.client.get('/error/')
        self.assertEqual(response.status_code, 404)
