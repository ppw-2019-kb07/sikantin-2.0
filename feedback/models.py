from django.db import models


class SavedFeedback(models.Model):
    feedback = models.CharField(max_length=300)
    testimony = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
