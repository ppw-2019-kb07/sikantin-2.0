from django import forms
from feedback import models


class FeedbackForm(forms.Form):
    feedback = forms.CharField(
        label='Your Feedback : ',
        required=True,
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'type': 'text',
            'placeholder': "Give your feedback here! ^.^"})
    )
    testimony = forms.CharField(
        label='What\'s your thought about this website?',
        required=True,
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'type': 'text',
            'placeholder': "Share your testimony here! ^.^"})
    )


class Meta:
    model = models.SavedFeedback
    fields = ('feedback', 'testimony')
