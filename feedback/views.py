from django.shortcuts import render, redirect
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy
from .models import SavedFeedback
from .forms import FeedbackForm

from django.http import (
    HttpResponseNotAllowed, HttpResponseRedirect,
    HttpResponseForbidden, HttpResponseNotFound,
)
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, logout


class feedback_delete(DeleteView):
    model = SavedFeedback
    success_url = reverse_lazy('feedback:feedback')


def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)

        if 'id' in request.POST:
            SavedFeedback.objects.get(id=request.POST['id']).delete()
            return redirect('/')

        if form.is_valid():
            # feedback = request.POST['feedback']
            feedback = SavedFeedback(
                feedback=form.data['feedback'],
                testimony=form.data['testimony']
            )
            feedback.save()
            return redirect('/')

    else:
        form = FeedbackForm()

    feedback_content = {'events': SavedFeedback.objects.all().values(),
                        'form': form}
    return render(request, 'feedback.html', feedback_content)



