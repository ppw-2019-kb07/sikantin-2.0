from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'feedback'

urlpatterns = [
    path('', views.feedback, name='feedback'),
    url(r'feedback/(?P<pk>[0-9]+)/delete/$',
        views.feedback_delete.as_view(), name='feedback_delete')
]
