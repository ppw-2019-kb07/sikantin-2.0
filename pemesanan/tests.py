from django.test import TestCase
from django.test import Client
from io import BytesIO
from PIL import Image
from django.core.files.base import File
from .models import OrderCounter,Orderedfood,Food,Order
from .forms import SearchForm,AmountForm,OrderForm
from django.contrib.auth.models import User
from datetime import date
# Create your tests here.
def get_image_file(name='test.png', ext='png', size=(50, 50), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)

class UnitTest(TestCase):
    def setUp(self):
        self.user=User.objects.create_user("testuser","johndoe@foo.bar","pepewe123",first_name="First", last_name="Last")
        self.client=Client()
        self.client.login(username="testuser",password="pepewe123")
    
        
    def test_form_is_valid(self):
        form_data = {'keyword':'Kata Kunci'}
        form = SearchForm(data = form_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['keyword'], 'Kata Kunci')
    
    def test_form_is_not_valid(self):
        form = SearchForm(data = {})
        self.assertFalse(form.is_valid())

    #def test_form_validity(self)
    # def test_view_index(self):
    #     found = resolve('')
    #     self.assertEqual(found.func, index)
    def test_increment_function_works(self):
        obj=Food.objects.create(food_id=1,name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.save()
        cnt=OrderCounter.objects.create(counter=1)
        cnt.save()
        obj=Food.objects.get(food_id=1)
        self.assertEqual(obj.food_id,1)
        response=self.client.get("/pemesanan/item/counter/1/")
        obj=Food.objects.get(food_id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(obj.counter,1)

    def test_increment_ofnotexist_function_works(self):
        obj=Food.objects.create(food_id=1,name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.save()
        cnt=OrderCounter.objects.create(counter=1)
        cnt.save()
        obj=Food.objects.get(food_id=1)
        order=Order(order_id=1)
        order.save()
        self.assertEqual(obj.food_id,1)
        response=self.client.get("/pemesanan/item/counter/1/")
        obj=Food.objects.get(food_id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(obj.counter,1)

    def test_addOrder_works(self):
        objCounter=OrderCounter.objects.create(counter=1)
        objCounter.save()
        response=self.client.get("/pemesanan/addFood/")
        self.assertEqual(response.status_code,200)
        obj=Order.objects.create(order_id=1)
        obj.save()
        response=self.client.get("/pemesanan/addFood/")
        self.assertEqual(response.status_code, 302)
        obj=OrderCounter.objects.get(pk=1)
        self.assertEqual(obj.counter,2)
    
    
    def test_home_exist(self):
        response=self.client.get("/pemesanan/")
        self.assertEqual(response.status_code,200)
    
    @classmethod

   
    def get_test_image_file():
        from django.core.files.images import ImageFile
        file = tempfile.NamedTemporaryFile(suffix='.png')
        return ImageFile(file, name=file.name)

    def test_food_pk(self):
        obj=Food.objects.create(name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.img=get_image_file()
        obj.save()
        self.assertEqual(obj.pk,1)
    
    def test_food_name(self):
        obj=Food.objects.create(name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.img=get_image_file()
        obj.save()
        obj=Food.objects.get(pk=1)
        self.assertEqual(obj.name,"makanan")
    
    def test_if_new_is_in_database(self):
        obj=Food.objects.create(name="makanan",merchant="toko",price=0,calories=0,counter=0)
        count = Food.objects.all().count()
        self.assertEqual(count, 1)

    def test_amount_request(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        order=Order.objects.get(order_id=1)
        response = self.client.get('/pemesanan/amount/')
        self.assertEqual(response.status_code, 200)

    def test_pembayaran_load(self):
        response = self.client.get('/pemesanan/pembayaran/')
        self.assertEqual(response.status_code, 200)
    
    def test_summary(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        response = self.client.get('/pemesanan/summary/')
        self.assertEqual(response.status_code, 200)
    
    def test_form_submit(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        c=Client()
        response=self.client.post('/pemesanan/submit/',{'date':date(2019,10,10),'phone_number':'081288915802','address':'pacil','delivery':True,'payment':'Cash'})
        order=Order.objects.get(order_id=1)
        self.assertEqual(response.status_code,302)
        self.assertEqual(order.date,date(2019,10,10))
        self.assertEqual(order.phone_number,'081288915802')
        self.assertEqual(order.address,'pacil')
        self.assertEqual(order.payment,'Cash')
        self.assertTrue(order.delivery)


