$(document).ready(function(){
    var topNews = {}
    var isi = ""
    $.ajax({
        url: "https://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=fd8f488b82e3441688b95a3dd7317076",
        success: function(result) {
            $("#News").empty();
            var angka = Math.floor(Math.random() * 11);
            $.each(result.articles, function (index, data){
                if ( angka < index && index < angka + 5 ) {
                    isi =   '<div class="col-md-3">'+
                                '<div class="" style="width: 18rem;">'+
                                '<img src="' + data.urlToImage + '" class="card-img-top" style="height: 15rem;">'+
                                '<div class="" style="height: 15rem;">' +
                                    '<h5 class=""><a href="' + data.url + '" style="text-decoration: none;color:black;">' + data.title + '</a></h5>' +
                                    '<p class="">'+ data.description + '</p>' +
                                '</div>'+
                                '</div>'+
                            '</div>'
                        // alert(data.title)
                    $("#News").append(isi);                            
                }
            });
        }
    });
});