from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
# Create your models here.
class Food(models.Model):
    food_id=models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    merchant = models.CharField(max_length=50)
    price=models.IntegerField(blank=True,null=True)
    calories=models.DecimalField(max_digits=10,decimal_places=2,null=True)
    counter = models.PositiveIntegerField(default=0, blank=True, null=True)
    img=models.ImageField(upload_to="media",null=True,blank=True)


class Search(models.Model):
  keyword=models.CharField(max_length=50,null=True,blank=True)
  
class Order(models.Model):
  order_id=models.IntegerField(primary_key=True, default=0)
  payment=models.CharField(blank=True,null=True,max_length=7)
  delivery=models.BooleanField(default=False)
  date=models.DateField(blank=True,null=True)
  user=models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
  phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
  phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
  address=models.TextField(blank=True,null=True)

class Orderedfood(models.Model):
    curr_id=models.PositiveIntegerField(default=1,null=True,blank=True)
    amount=models.PositiveIntegerField(default=1,null=True,blank=True)
    attr=models.OneToOneField(Food,on_delete=models.CASCADE,primary_key=True)
    order=models.ForeignKey(Order,on_delete=models.CASCADE,null=True,blank=True)

class OrderCounter(models.Model):
  counter=models.IntegerField(default=1)
