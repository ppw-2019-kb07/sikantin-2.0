from django.contrib import admin
from .models import Food,Search,Order,OrderCounter,Orderedfood
# Register your models here.
admin.site.register(Food)
admin.site.register(Search)
admin.site.register(Order)
admin.site.register(OrderCounter)
admin.site.register(Orderedfood)