from django.shortcuts import render,redirect
from .forms import OrderForm,AmountForm,SearchForm
from django.views.generic import TemplateView, ListView
from .models import Order,OrderCounter,Orderedfood,Food,Search
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.core.mail import send_mail
from django.conf import settings
from history.models import History
import random
@login_required
# Create your views here.
def increment(request,item_id):
    object = Food.objects.get(food_id=item_id)
    try:
        obj=OrderCounter.objects.get(pk=1)
        curr_id=obj.counter
        order=Order.objects.get(order_id=curr_id)
        ordered_food=Orderedfood.objects.get(curr_id=curr_id,order=order,attr=object)
    except Order.DoesNotExist:
        order=Order(order_id=curr_id)
        order.save()
        ordered_food=Orderedfood(curr_id=curr_id,order=None,attr=object)
        ordered_food.save()
    except Orderedfood.DoesNotExist:
        ordered_food=Orderedfood(curr_id=curr_id,order=None,attr=object)
        ordered_food.save()
    finally:
        ordered_food=Orderedfood.objects.get(curr_id=curr_id,attr=object)
        if(not ordered_food.order==order):
            object.counter=object.counter+1
            object.save()
            ordered_food.order=order
            ordered_food.save()
    return redirect('/pemesanan/')

@login_required
def new_order(request):
    try:
       obj=OrderCounter.objects.get(pk=1)
       curr_id=obj.counter
       order=Order.objects.get(order_id=curr_id)
       obj.counter=obj.counter+1
       obj.save()
       return redirect('/pemesanan/amount')
    except Order.DoesNotExist:
        return HttpResponse("Anda belum memesan apapun :(")
     
@login_required
def homepage(request):
    response = {"search_form": SearchForm}
    response['first_half']=Food.objects.all()[:len(Food.objects.all())//2]
    response['second_half']=Food.objects.all()[len(Food.objects.all())//2:]
    response['recommendations']=Food.objects.all().order_by('-counter')[:3]
    return render(request, 'home.html', response)

@login_required
def search_results(request):
    response={}
    if request.method=="GET":
        form = SearchForm(request.GET)
        if (form.is_valid()):
            keyword = form.cleaned_data['keyword']
            search=Search(keyword=keyword)
            search.save()
            out=Food.objects.filter(name__icontains=search.keyword)
            response['foods']=out
    return render(request,'search_results.html',response)

@login_required
def index_amount(request):
    response = {"amount_form": AmountForm}
    counter=OrderCounter.objects.get(pk=1)
    counter_val=counter.counter-1
    order=Order.objects.get(order_id=counter_val)
    out=order.orderedfood_set.all()
    response['query']=out
    response['user']=request.user
    return render(request,'amount.html',response)

@login_required
def set_amount(request,curr_id=None,food_id=None):
    response={}
    food=Food.objects.get(food_id=food_id)
    of=Orderedfood.objects.get(curr_id=curr_id,attr=food)
    if(request.method == 'POST'):
        form = AmountForm(request.POST)
        if (form.is_valid()):
            form_content = form.cleaned_data['amount']
            of.amount=form_content
            of.save()
            #form.save()
            #asresponse['amount_form']=form
    #return render(request,'amount.html',response)
    return redirect('/pemesanan/amount')

@login_required
def index_payment(request):
    response={'form':OrderForm,'user':request.user}
    return render(request,'pembayaran.html',response)

@login_required
def submit_form(request):
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    if(request.method == 'POST'):
        form = OrderForm(request.POST)
        if (form.is_valid()):
            date=form.cleaned_data['date']
            phone_number=form.cleaned_data['phone_number']
            address=form.cleaned_data['address']
            delivery=form.cleaned_data['delivery']
            payment=form.cleaned_data['payment']
            order.payment=payment
            order.delivery=delivery
            order.date=date
            order.user=request.user
            order.phone_number=phone_number
            order.address=address
            order.save()
    return redirect('/pemesanan/pembayaran/')

@login_required 
def summary(request):
    response={}
    delivery_fee= random.randint(1000, 15000) 
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    if(order.payment==None or order.date==None or order.address==None or order.phone_number==None):
        return render(request,'pleasefilldata.html')
    response['order']=order
    out=order.orderedfood_set.all()
    total_price=0
    for item in out:
        total_price=total_price+item.attr.price*item.amount
    response['food']=out
    if(order.delivery==False):
        delivery_fee=0
    total_price=total_price+delivery_fee
    response['delivery_fee']=delivery_fee
    response['total_price']=total_price
    response['name']=request.user.first_name+" "+request.user.last_name
    history=History(user=request.user, order=order,price=total_price)
    history.save()
    return render(request,'summary.html',response)

@login_required
def email(request):
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    subject = 'Customer Invoice'
    message = 'Thank you for using our service. Here\'s your invoice:\nNama:{} {}\nAlamat Pengambilan:{}\nTanggal Pengambilan:{}'.format(request.user.first_name,request.user.last_name,order.address,order.date)
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [request.user.email]
    send_mail( subject, message, email_from, recipient_list )
    return redirect('/pemesanan/')