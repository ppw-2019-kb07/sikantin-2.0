from django.contrib import admin
from django.urls import path
from .views import increment,new_order,index_amount,set_amount,index_payment,summary,submit_form,summary,homepage,search_results,email
urlpatterns = [
    path('search/', search_results, name='search_results'),
    path('', homepage, name='home'),
    path('item/counter/<int:item_id>/',increment,name="increment"),
    path('addFood/',new_order,name="new_order"),
    path('amount/', index_amount,name="index_amount"),
    path('set_amount/<int:curr_id>/<int:food_id>/',set_amount,name="set_amount"),
    path('pembayaran/', index_payment,name="index_payment"),
    path('submit/',submit_form,name='submit_form'),
    path('summary/',summary,name='summary'),
    path('send/',email,name='email'),
]

