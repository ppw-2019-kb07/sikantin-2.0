from django.shortcuts import render, redirect
from django.http import HttpResponse,JsonResponse
# Create your views here.
from django.contrib.auth.decorators import login_required
from pemesanan.models import Order,OrderCounter,Orderedfood
from .forms import CalorieForm,SearchFoodForm
from .models import Calorie
import requests
import json
from django.conf import settings


def food_index(request):
    form=SearchFoodForm()
    return render(request,'searchFood.html',{'form':form,'user':request.user})

def getFood(request):
    response = {}
    responseCode = 0
    food=request.GET.get('query',None)
    cal=request.GET.get('cal',None)
    #key=settings.FOODAPI_KEY
    key='8313377d94024b0ab5a1b39dad727e5a'
    url = 'https://api.spoonacular.com/recipes/complexSearch?query={}&minCalories={}&apiKey={}'.format(food,cal,key)
    
    return JsonResponse(requests.get(url).json())



@login_required
def index(request):
    response={'form':CalorieForm}
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    ans=0
    try:
        out=order.orderedfood_set.all()
    except:
        return redirect("homepage")
    for item in out:
        ans+=(item.attr.calories*item.amount)
    try:
        calorie=Calorie.objects.get(pk=1)
    except Calorie.DoesNotExist:
        calorie=Calorie(total_calories=0)
        calorie.save()
    finally:
        calorie=Calorie.objects.get(pk=1)
        calorie.total_calories=ans
        calorie.save()
    return render(request,'kalori.html',response)

@login_required
def get_kalori(request):
    response={'output':'test'}
    output='test'
    age=request.GET.get('age',None)
    activity=request.GET.get('activity',None)
    gender=request.GET.get('gender',None)
    calorie=Calorie.objects.get(pk=1)
    calorie_value=calorie.total_calories
    low_limit=0
    high_limit=0
    if(gender=='Pria'):
        if(age=='Remaja'):
            if(activity=='Tidak Terlalu Aktif'):
                low_limit=2400
                high_limit=2400
            elif(activity=='Lumayan Aktif'):
                low_limit=2600
                high_limit=2800
            elif(activity=='Sangat Aktif'):
                low_limit=3000
                high_limit=3000
        elif(age=='Dewasa'):
            if(activity=='Tidak Terlalu Aktif'):
                low_limit=2200
                high_limit=2200
            elif(activity=='Lumayan Aktif'):
                low_limit=2400
                high_limit=2600
            elif(activity=='Sangat Aktif'):
                low_limit=2800
                high_limit=3000
        else:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            if(activity=='Tidak Terlalu Aktif'):
                low_limit=2000
                high_limit=2000
            elif(activity=='Lumayan Aktif'):
                low_limit=2200
                high_limit=2400
            elif(activity=='Sangat Aktif'):
                low_limit=2400
                high_limit=2800
            
    else:
        if(age=='Remaja'):
            if(activity=='Tidak Terlalu Aktif'):
                low_limit=2000
                high_limit=2000
            elif(activity=='Lumayan Aktif'):
                low_limit=2000
                high_limit=2200
            elif(activity=='Sangat Aktif'):
                low_limit=2400
                high_limit=2400
        elif(age=='Dewasa'):
            if(activity=='Tidak Terlalu Aktif'):
                low_limit=1800
                high_limit=1800
            elif(activity=='Lumayan Aktif'):
                low_limit=2000
                high_limit=2000
            elif(activity=='Sangat Aktif'):
                low_limit=2200
                high_limit=2200
        else:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            if(activity=='Tidak Terlalu Aktif'):
                low_limit=1600
                high_limit=1600
            elif(activity=='Lumayan Aktif'):
                low_limit=1800
                high_limit=1800
            elif(activity=='Sangat Aktif'):
                low_limit=2000
                high_limit=2200
    if(low_limit==0 or high_limit==0):
        output="Terjadi error dalam pemrosesan"
    if(calorie_value >= low_limit and calorie_value <=high_limit):
        output="Kalori Anda sudah terpenuhi"
    elif(calorie_value > high_limit):
        output="Makanan yang Anda pesan sudah melebihi {} dari batas kalori".format(calorie_value-high_limit)
    else:
        output="Makanan yang Anda pesan masih kurang {} dari batas kalori".format(low_limit-calorie_value)
    response={'output':output}
    return JsonResponse(response) 

