from django.contrib import admin
from .models import Calorie
# Register your models here.
class CalorieAdmin(admin.ModelAdmin):
    list_display = ['total_calories']
admin.register(Calorie,CalorieAdmin)