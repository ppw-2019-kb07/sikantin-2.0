from django import forms
class CalorieForm(forms.Form):
    ACTIVITY=(
        ('Tidak Terlalu Aktif','Tidak Terlalu Aktif'),('Lumayan Aktif','Lumayan Aktif'),('Sangat Aktif','Sangat Aktif')
    )
    GENDER=(
        ('Pria','Pria'),('Wanita','Wanita')
    )
    AGE=(
        ('Remaja','Remaja'),('Dewasa','Dewasa'),('Lansia','Lansia')
    )
    age=forms.ChoiceField(choices=AGE,required=False,label="Jenjang Umur")
    gender=forms.ChoiceField(choices=GENDER,required=False,label="Jenis Kelamin")
    activity=forms.ChoiceField(choices=ACTIVITY,required=False,label="Frekuensi Aktivitas")

class SearchFoodForm(forms.Form):
    attrs1 = {
        'type':'query',
        'class': 'form-control',
        'id':'search1-query',
        'placeholder':'Masukkan nama makanan di sini'
    }
    attrs2 = {
        'type':'query',
        'class': 'form-control',
        'id':'search2-query',
        'placeholder':'Masukkan kalori minimum di sini'
    }
    keyFood = forms.CharField(required=False,label='Nama Makanan', widget=forms.TextInput(attrs=attrs1))
    keyCalorie=forms.CharField(required=False,label='Kalori Minimum', widget=forms.TextInput(attrs=attrs2))
