
from django.test import TestCase,Client
from pemesanan.models import Order,OrderCounter,Orderedfood,Food
from .models import Calorie
from .forms import CalorieForm
from django.contrib.auth.models import User
from django.urls import resolve
from .views import getFood
# Create your tests here.

class KaloriModelTest(TestCase):
    def test_search_food(self):
        found=resolve('/search/get-food/')
        self.assertEqual(found.func, getFood)  
        client = Client()
        response = client.post('/search/get-food/', data={"cal" : "0", "query" : "Epstein didnt kill himself"})
        self.assertEqual(response.status_code, 200)

    def test_url_search_error(self):
        client=Client()
        response=client.get('/search/error/')
        self.assertEqual(response.status_code, 404)

    def test_url_exist(self):
        client=Client()
        response=client.get('/search/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'searchFood.html')

    def setUp(self):
        self.user=User.objects.create_user("testuser","johndoe@foo.bar","pepewe123",first_name="First", last_name="Last")
        self.client=Client()
        self.client.login(username="testuser",password="pepewe123")

    def test_index_calorie_exist_functionality(self):
        calorie=Calorie(total_calories=0)
        calorie.save()
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        order=Order.objects.get(order_id=1)
        response = self.client.get('/kalori/')
        self.assertEqual(response.status_code, 200)
        calorie=Calorie.objects.get(pk=1)
        self.assertEqual(calorie.total_calories,4.00)
        
    def test_index_calorie_notexist_functionality(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        order=Order.objects.get(order_id=1)
        response = self.client.get('/kalori/')
        self.assertEqual(response.status_code, 200)
        calorie=Calorie.objects.get(pk=1)
        self.assertEqual(calorie.total_calories,4.00)
    
    def test_calorie_response(self):
        calorie=Calorie(total_calories=2200)
        calorie.save()
        response=self.client.post('/kalori/getkalori/',{'age':'Dewasa','activity':'Tidak Terlalu Aktif','gender':'Pria'})
        self.assertEqual(response.status_code,200)
        response=self.client.post('/kalori/getkalori/',{'age':'Lansia','activity':'Tidak Terlalu Aktif','gender':'Pria'})
        self.assertEqual(response.status_code,200)
        response=self.client.post('/kalori/getkalori/',{'age':'Remaja','activity':'Sangat Aktif','gender':'Pria'})
        self.assertEqual(response.status_code,200)

   
