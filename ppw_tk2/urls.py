"""ppw_tk2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from history.views import homepage,signup,login,logout
from kalori.views import food_index,getFood
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',homepage,name='homepage'),
    path('pemesanan/',include('pemesanan.urls')),
    path('history/', include('history.urls')),
    path('feedback/',include('feedback.urls')),
    path('signup/', signup, name='signup'),
    path('login/', login, name='login'),
    path('kalori/',include('kalori.urls')),
    path('logout/',logout, name='logout'),
    
    #path('news/',news_index,name="news"),
    #path('news/get-news/',getNews,name='get-news'),
    path('search/',food_index,name="searchFood"),
    path('search/get-food/',getFood,name='get-food')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
