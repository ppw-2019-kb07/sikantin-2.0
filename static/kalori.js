$(document).ready(function(){
    show();
    getNews(); 

})

function getNews() {
    $.ajax({
        url:'127.0.0.1:8000/news/get-news/',
        data:
        success:function (result) {
            if ($.trim($("#news-content").html()).length != 0 ) {
                $("#news-content").empty();
            }
            var complete_text = ""
            for (i = 0; i < result.totalResults; i++) {
                var url = result.articles[i].url;
                var urlToImage =  result.articles[i].urlToImage;
                var title =  result.articles[i].title;
                var description =  result.articles[i].description;

                complete_text +=
                    '<a href="' + url + '">'+
                            '<img class="" src="' + urlToImage + '" alt="">'+
                                '<div class="overlay">'+
                                    '<h2>' + title +'</h2>'+
                                    '<p>'+ description + '</p>'+
                                '</div>'+                    
                        '</a>'+
                    '</div>'+
                '</div>';
            }
            var container_open = '<div class="d-flex justify-content-around align-items-center flex-wrap">';
            var container_close = '</div> <div class="text-center"><p>powered by <a class="text-dark font-weight-bold" href="https://newsapi.org">newsapi.org</a></p></div>';
            $("#news-content").html(container_open+ complete_text + container_close);
        }
    })
}
function show() {
    $('.jsonly').css('display','block');
}


