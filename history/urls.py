from django.contrib import admin
from django.urls import path
from .views import history
urlpatterns = [
    path('', history,name="history")
]