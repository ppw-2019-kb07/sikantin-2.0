from django.db import models
from django.contrib.auth.models import User
from pemesanan.models import Order
# Create your models here.
class History(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    order=models.OneToOneField(Order,on_delete=models.CASCADE,null=True,blank=True)
    price=models.IntegerField(blank=True,null=True)
