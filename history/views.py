from django.contrib.auth import login as user_login, logout as user_logout, authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import LoginForm,SignUpForm
from .models import History

# Create your views here.
def anonymous_required(view, redirect_url='/history'):
    actual_decorator = user_passes_test(lambda u: u.is_anonymous,login_url=redirect_url)
    return actual_decorator(view)

@anonymous_required
def homepage(request):
    return render(request,'homepage.html',{'user':request.user})
    '''
    reviews=Reviews.objects.all()
    return render(request,'homepage.html',{'reviews':Reviews.objects.all()})
    '''

@anonymous_required
def login(request):
    if request.method=='POST':
        form=LoginForm(request.POST)
        if form.is_valid():
            if form.login_granted:
                user_login(request,form.user)
                return redirect('/history/')
    else:
        form=LoginForm()
    return render(request,'login.html',{'form':form})


@anonymous_required
def signup(request):
    if request.method=='POST':
        form=SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login/')
    else:
        form=SignUpForm()
    return render(request,'signup.html',{'form':form})


@login_required
def logout(request):
    user_logout(request)
    return redirect('/')

@login_required
def history(request):
    hist=request.user.history_set.all()
    name=request.user.first_name+" "+request.user.last_name
    return render (request,'history.html',{'user':name,'orders':hist})
    #return HttpResponse('Anda sudah login')