from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
class SignUpForm(UserCreationForm):
    username=forms.CharField(max_length=20,required=True,label='Nama Pengguna')
    first_name=forms.CharField(max_length=100,required=True,label='Nama Depan')
    last_name=forms.CharField(max_length=50,required=True,label='Nama Belakang')
    email = forms.EmailField(max_length=100,required=True)

    class Meta:
        model = User
        fields = ['username', 'email','first_name', 'last_name','password1', 'password2']

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        for field in SignUpForm.Meta.fields:
            self.fields[field].help_text = None

class LoginForm(forms.Form):
    username=forms.CharField(label='Nama Pengguna')
    password = forms.CharField(label='Sandi', widget=forms.PasswordInput)
    def clean(self):
        cleaned_data=super().clean()
        username=cleaned_data.get('username')
        password=cleaned_data.get('password')
        if username and password:
            self.user=authenticate(username=username,password=password)
            if self.user:
                self.login_granted=True
            else:
                self.add_error('username','Nama Pengguna atau Sandi yang Anda masukkan salah')
                self.add_error('password','Nama Pengguna atau Sandi yang Anda masukkan salah')

