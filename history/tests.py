from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
# Create your tests here.
class UnitTest(TestCase):
    def setUp(self):
        self.user=User.objects.create_user("testuser","johndoe@foo.bar","pepewe123",first_name="First", last_name="Last")
        self.client=Client()
        self.client.login(username="testuser",password="pepewe123")
    
    def test_homepage_response(self):
        client=Client()
        response=client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_response(self):
        client=Client()
        response=client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_response(self):
        client=Client()
        response=client.get('/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_error_page(self):
        client=Client()
        response=client.get('/error/')
        self.assertEqual(response.status_code, 404)
    
    def test_get_signup(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_post_signup(self):
        response = Client().post('/signup/',{'username':'JD','first_name':'John','lat_name':'Doe','password1':'foobar123456','password2':'foobar123456','email':'johndoe@foo.bar'},)
        self.assertIn('JD',response.content.decode())

    def test_post_login(self):
        response = Client().post('/login/',{
            'username':'nezuko',
            'password':'user1234567',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        user=User.objects.create_user("test","johndoe@foo.bar","pepewe123",first_name="First", last_name="Last")
        user.save()
        response = Client().post('/login/',{
            'username':'test',
            'password':'pepewe123',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_history(self):
        response=self.client.get('/history/')
        self.assertEqual(response.status_code,200)
        
    def test_logout(self):
        response = self.client.get('/logout/', follow=True)
        self.assertEqual(response.status_code, 200)




